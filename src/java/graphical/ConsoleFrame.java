package graphical;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Benjamin
 */
public class ConsoleFrame extends JFrame {

    private JTextArea ta;

    public ConsoleFrame(String title, boolean manager) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ta = new JTextArea();//, 6, 8);
        ta.setLineWrap(true);
        ta.setWrapStyleWord(true);
        ta.setEditable(false);
        
JScrollPane scrollPane = new JScrollPane(ta);


        add(scrollPane);
        pack();
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int x = 0;
        int y = 0;
        if (manager) {
            setSize(500, (int) d.getHeight() - 40);
            x = (int) d.getWidth() - getWidth();
            y = 0;

        } else {
            setSize(400, 300);
            Random r = new Random();
            x = r.nextInt((int) (d.getWidth() - 500 - getWidth()));
            y = r.nextInt((int) (d.getHeight() - getHeight()));
        }
        setLocation(x, y);
        setVisible(true);
    }

    public void print(String text) {
        ta.append(text + '\n');
        ta.setCaretPosition(ta.getDocument().getLength());
    }
}
