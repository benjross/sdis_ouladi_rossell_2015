package manageQueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.util.Random;

/**
 *
 * @author Benjamin
 */
public class Sender {

    private static final String EXCHANGE_NAME = "messagesQueue";
    private String message = "";
    private String charge = "1";

    public Sender(String c, String m) {
        message = m;
        charge = c;
    }

    public String send()
            throws java.io.IOException {
        ConnectionFactory factory = new ConnectionFactory();
        //factory.setHost("localhost");
        factory.setHost("ec2-54-73-194-42.eu-west-1.compute.amazonaws.com");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        channel.basicPublish(EXCHANGE_NAME, "toDo", null, (charge + '#' + message).getBytes());
        System.out.println(" [x] Sent '" + "toDo" + "':'" + charge + '#' + message + "'");
        channel.close();
        connection.close();
        String returnedMessage = "";
        if (message.equals("stop")) {
            returnedMessage += "The order to stop the experiment as been sent.";
        } else {
            returnedMessage += "Message sent : " + message + " with a charge of " + charge;
        }
        return returnedMessage;
    }
}
