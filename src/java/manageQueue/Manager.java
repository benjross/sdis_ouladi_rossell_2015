package manageQueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import graphical.ConsoleFrame;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Benjamin
 */
public class Manager {

    private static final String EXCHANGE_NAME = "messagesQueue";
    private static final int WORKER_CAPACITY = 30;
    private static final ConsoleFrame ManagerFrame = new ConsoleFrame("Manager", true);
    private static final ArrayList<Worker> workers = new ArrayList<>();
    private static final ArrayList<Task> tasks = new ArrayList<>();
    private static int numberOfTasks = 0;

    public static void main(String[] argv)
            throws java.io.IOException,
            java.lang.InterruptedException {

        ConnectionFactory factory = new ConnectionFactory();
        //factory.setHost("localhost");
        factory.setHost("ec2-54-73-194-42.eu-west-1.compute.amazonaws.com");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, EXCHANGE_NAME, "toDo");
        channel.queueBind(queueName, EXCHANGE_NAME, "created");
        channel.queueBind(queueName, EXCHANGE_NAME, "done");

        print(" [Manager] Maximum charge loaded for the workers : " + WORKER_CAPACITY);
        print(" [Manager] Waiting for messages.");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, true, consumer);
        
        boolean stop = false;
        while (!stop) {
            print("\n [Manager] List of Workers : \n"+workers.toString());
            print(" [Manager] List of Tasks : \n"+tasks.toString());
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());
            String routingKey = delivery.getEnvelope().getRoutingKey();
            print(" [Manager] Received the key '" + routingKey + "' for the message : '" + message + "'");
            int chg = Integer.parseInt(message.split("#")[0]);
            String msg = message.split("#")[1];
            if (msg.equals("stop")) {
                stop = true;
                stop(channel);
            } else if (delivery.getEnvelope().getRoutingKey().equals("created")) {
                int num = chg;
                print(" [Manager] Worker " + num + " is ready !");
                dispatchTasks(channel);
            } else if (delivery.getEnvelope().getRoutingKey().equals("done")) {
                int no = chg;
                int num = Integer.parseInt(msg);
                print(" [Manager] Task " + no + " done by Worker " + msg);
                removeTaskDone(no, num, channel);
            } else {
                if (chg > WORKER_CAPACITY) {
                    chg = WORKER_CAPACITY;
                }
                addTask(chg, msg);
                dispatchTasks(channel);
            }
        }
        channel.close();
        connection.close();
        print(" [Manager] End");
        ManagerFrame.dispose();
    }

    public static void send(int no, int charge, String message, int workerNum, Channel channel)
            throws java.io.IOException {
        channel.basicPublish(EXCHANGE_NAME, Integer.toString(workerNum), null, (Integer.toString(no) + '#' + Integer.toString(charge) + '#' + message).getBytes());
        print(" [Manager] Sent to Worker " + workerNum + " : " + Integer.toString(no) + '#' + Integer.toString(charge) + '#' + message + "");
    }

    public static void print(String text) {
        System.out.println(text);
        try {
            ManagerFrame.print(text);
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
        }
    }

    public static Worker addWorker() {
        Worker w = new Worker(workers.size() +1, 0);
        workers.add(w);
        print(" [Manager] Added Worker " + w.getNum());
        return w;
    }

    public static void removeWorker(int no) {
        boolean found = false;
        int leng = workers.size();
        int i = 0;
        while (!found && i < leng) {
            if (workers.get(i).getNum() == no) {
                workers.remove(i);
                print(" [Manager] Removed Worker " + Integer.toString(no));
                found = true;
            }
            i++;
        }
    }

    public static int addTask(int c, String m) {
        numberOfTasks += 1;
        Task t = new Task(numberOfTasks, c, m);
        tasks.add(t);
        return numberOfTasks;
    }

    public static void stop(Channel channel) throws IOException {
        print(" [Manager] Good night !");
        for (Worker w : workers) {
            send(0, 0, "stop", w.getNum(), channel);
        }
    }


    public static void removeTaskDone(int no, int num, Channel channel) throws IOException {
        boolean found = false;
        int leng = tasks.size();
        int i = 0;
        while (!found && i < leng) {
            Task t = tasks.get(i);
            if (t.getNo() == no) {
                boolean foundW = false;
                int lengW = workers.size();
                int iW = 0;
                while (!foundW && iW < lengW) {
                    Worker w = workers.get(iW);
                    if (w.getNum() == num) {
                        workers.get(iW).releaseCharge(t.getCharge());
                        print(" [Manager] Released Worker " + Integer.toString(num) + " from " + t.getCharge() + ", now charged for  " + w.getChargeLoaded());
                        if (w.getChargeLoaded() == 0) {
                            print(" [Manager] Worker " + Integer.toString(num) + " totally free, it's time to revoke it !");
                            send(0, 0, "stop", num, channel);
                            removeWorker(w.getNum());
                        }
                        foundW = true;
                    }
                    iW++;
                }
                tasks.remove(i);
                print(" [Manager] Removed task " + Integer.toString(no));
                found = true;
            }
            i++;
        }
    }

    public static void dispatchTasks(Channel channel) throws IOException {
        for (Task t : tasks) {
            boolean everyWorkerIsBusy = true;
            if (t.hasNotBeenDispatched()) {
                int no = t.getNo();
                int chg = t.getCharge();
                String msg = t.getMessage();
                for (Worker w : workers) {
                    if (t.hasNotBeenDispatched() && ((w.getChargeLoaded() + chg) <= WORKER_CAPACITY)) {
                        everyWorkerIsBusy = false;
                        int num = w.getNum();
                        send(no, chg, msg, num, channel);
                        t.setDispatched(num);
                        w.loadCharge(chg);
                        print(" [Manager] Dispatched task " + Integer.toString(no) + " to Worker " + Integer.toString(num) + ", now charged for " + w.getChargeLoaded());

                    }
                }
                if (everyWorkerIsBusy) {
                    Worker w1 = addWorker();
                    int num = w1.getNum();
                    print(" [Manager] There are not enough workers ! Creation of the worker " + Integer.toString(num));

                }
            }

        }
    }
}
