package manageQueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import graphical.ConsoleFrame;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Benjamin
 */
public class Worker implements Runnable {

    private static final String EXCHANGE_NAME = "messagesQueue";
    private int num = 0;
    private int chargeLoaded = 0;
    private ConsoleFrame WorkerFrame;

    public Worker(int n, int c) {
        num = n;
        chargeLoaded = c;
        WorkerFrame = new ConsoleFrame("Worker " + num, false);
        this.start();
    }

    public void listen()
            throws java.io.IOException,
            java.lang.InterruptedException {

        ConnectionFactory factory = new ConnectionFactory();
        //factory.setHost("localhost");
        factory.setHost("ec2-54-73-194-42.eu-west-1.compute.amazonaws.com");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, EXCHANGE_NAME, Integer.toString(num));

        print(" [Worker " + num + "] Waiting for messages.");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, true, consumer);

        boolean stop = false;
        channel.basicPublish(EXCHANGE_NAME, "created", null, (Integer.toString(num) + "#IamReady").getBytes());

        while (!stop) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());
            int no = Integer.parseInt(message.split("#")[0]);
            int chg = Integer.parseInt(message.split("#")[1]);
            String msg = message.split("#")[2];
            if (msg.equals("stop")) {
                stop = true;
            } else {
                print(" [Worker " + num + "] I am beginning to work on the task " + no + ", it will last for " + chg + " seconds and the message is " + msg);
                Thread.sleep(chg * 1000);
                send(no, chg, msg, num, channel);
                print(" [Worker " + num + "] I finished to work on the task " + no + ", it took me " + chg + " seconds to work on the message " + msg);
            }
        }
        channel.close();
        connection.close();
        print(" [Worker " + num + "] End");
        WorkerFrame.dispose();
    }

    private Thread t;

    public void run() {
        print(" [Worker " + num + "] Running " + num);
        try {
            listen();
        } catch (InterruptedException e) {
            print(" [Worker " + num + "] Thread " + num + " interrupted.");
        } catch (IOException ex) {
            Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
        }
        print(" [Worker " + num + "] Thread " + num + " exiting.");
    }

    public void start() {
        print(" [Worker " + num + "] Starting " + num);
        if (t == null) {
            t = new Thread(this, Integer.toString(num));
            t.start();
        }
    }

    public void print(String text) {
        System.out.println(text);
        try {
            WorkerFrame.print(text);
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
        }
    }

    public void send(int no, int charge, String message, int workerNum, Channel channel)
            throws java.io.IOException {
        channel.basicPublish(EXCHANGE_NAME, "done", null, (Integer.toString(no) + '#' + Integer.toString(num)).getBytes());
        this.print(" [Worker " + num + "] Done Task" + Integer.toString(no) + " in " + Integer.toString(charge) + " sec : " + message);
    }

    public int getNum() {
        return num;
    }

    public int getChargeLoaded() {
        return chargeLoaded;
    }

    public void loadCharge(int c) {
        chargeLoaded += c;
    }

    public void releaseCharge(int c) {
        chargeLoaded -= c;
    }
    public String toString(){
        return "\t[Worker " + num + ", charged for " + chargeLoaded + "]\n";
    }
}
