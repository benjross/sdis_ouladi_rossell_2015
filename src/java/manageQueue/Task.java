package manageQueue;

/**
 *
 * @author Benjamin
 */
public class Task {

    private int No; // id number of the task
    private int Charge; // charge of the task
    private String Message; // message held in the task
    private int Dispatched; // -1 if the task has not been dispatched to a specific worker yet

    public Task(int no, int c, String m) {
        No = no;
        Charge = c;
        Message = m;
        Dispatched = -1;
    }

    public int getNo() {
        return No;
    }

    public String getMessage() {
        return Message;
    }

    public int getCharge() {
        return Charge;
    }

    public String toString() {
        String res = "\t[Task " + No + ", " + Charge + " seconds, " + Message + ", DispatchedTo:";
        if (Dispatched == -1) {
            res += "Not yet";
        } else {
            res += Dispatched;
        }
        return res + "]\n";
    }

    public int getDispatched() {
        return Dispatched;
    }

    public void setDispatched(int d) {
        Dispatched = d;
    }

    public boolean hasNotBeenDispatched() {
        return this.getDispatched() == -1;
    }
}
