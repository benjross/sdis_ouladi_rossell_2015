package manage;

import java.io.IOException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import manageQueue.Sender;

/**
 *
 * @author Benjamin
 */
@WebService(serviceName = "manager")
public class manager {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "manage")
    public String manage(@WebParam(name = "msg") String msg, @WebParam(name = "chg") String chg) throws IOException {
        Sender s = new Sender(chg,msg);
        return s.send();
    }
}
