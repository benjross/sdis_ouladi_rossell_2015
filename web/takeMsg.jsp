<%-- 
    Document   : newjsp
    Created on : 29 janv. 2015, 17:48:59
    Author     : Benjamin
--%>

<%@page import="java.util.Random"%>
<%@page import="manage.manager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SDIS Ouladi Rossell 2015 : jsp</title>
    </head>
    <body>
        <div><b>WebService response : </b>
                <%
                    try {
                        if (request.getParameter("stop") != null && request.getParameter("stop").equals("true")) {
                            manage.manager service = new manage.manager();
                            out.println(service.manage("stop", "0"));
                        } else if (request.getParameter("random") != null && request.getParameter("random").equals("true")) {
                            manage.manager service = new manage.manager();
                            int length = 8;
                            String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            Random rng = new Random();
                            char[] text = new char[length];
                            for (int i = 0; i < length; i++) {
                                text[i] = characters.charAt(rng.nextInt(characters.length()));
                            }
                            Random randCharge = new Random();
                            int charge = randCharge.nextInt((20 - 1) + 1) + 1;
                            out.println(service.manage(new String(text), Integer.toString(charge)));
                        } else {
                            manage.manager service = new manage.manager();
                            String mes = request.getParameter("msg");
                            if (mes == null || mes.equals("")) {
                                mes = "foo";
                            }
                            String cha = request.getParameter("chg");
                            if (cha == null || cha.equals("")) {
                                cha = "1";
                            }
                            out.println(service.manage(mes, cha));
                        }
                    } catch (Exception ex) {
                        out.println("exception" + ex);
                    }
                %>
        </div>
        <div> <form action="takeMsg.jsp" method="POST">
                <fieldset>
                    <legend>Enter a message and an integer value for the working charge :</legend>
                    Message:<br>
                    <input type="text" name="msg" value="">
                    <br>
                    Charge:<br>
                    <input type="text" name="chg" value="">
                    <br><br>
                    <input type="submit" value="Send"></fieldset>
            </form> </div>
        <div> <form action="takeMsg.jsp" method="GET">
                <fieldset>
                    <legend>Or - You can generate a random set of values : </legend>
                    <input type="hidden" name="random" value="true" /> 
                    <input type="submit" value="Rand  !"></fieldset>
            </form> </div>
        <div> <form action="takeMsg.jsp" method="GET">
                <fieldset>
                    <legend>Or - You can also stop the experiment here : </legend>
                    <input type="hidden" name="stop" value="true" /> 
                    <input type="submit" value="Stop it !"></fieldset>
            </form> </div>
    </body>
</html>
